import { DataDedicatedAccount } from './../src/DataDedicatedAccount.js';

describe('Test Data Dedicated account', () => {

    test('Test creation', () => {
        expect(() => {
            let balance = 10;
            let unitCost = 5;
            let dataDedicatedAccount = new DataDedicatedAccount(balance, unitCost);
        }).not.toThrow();
    });

    test('Test get type', () => {
        expect(() => {
            let balance = 10;
            let unitCost = 5;
            let dataDedicatedAccount = new DataDedicatedAccount(balance, unitCost);
            
            expect(dataDedicatedAccount.getType()).toEqual('data');
        }).not.toThrow();
    });

    test('Test set type', () => {
        expect(() => {
            let balance = 10;
            let unitCost = 5;
            let dataDedicatedAccount = new DataDedicatedAccount(balance, unitCost);
            
            dataDedicatedAccount.setType('call');
        }).toThrow('Dedicated account can not change type!');
    });


    describe('test deduce Data', () => {
        test('sufficient balance', () => {
            expect(() => {
                let balance = 510;
                let unitCost = 5;
                let dataDedicatedAccount = new DataDedicatedAccount(balance, unitCost);
                
                let data = 100;
                dataDedicatedAccount.deduceData(data);

                expect(dataDedicatedAccount.getBalance()).toEqual(balance - unitCost * data);

            }).not.toThrow();
        });

        test('non-sufficient balance', () => {
            expect(() => {
                let balance = 250;
                let unitCost = 5;
                let dataDedicatedAccount = new DataDedicatedAccount(balance, unitCost);
                
                let data = 100;
                dataDedicatedAccount.deduceData(data);

            }).toThrow('data dedicated account does not have enough balance to perform this action!');
        });
    });

});