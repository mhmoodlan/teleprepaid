import { MainAccount } from './../src/MainAccount.js';


describe('test main account creation', () => {

    describe('test main account balance', () => {
        const CALL_COST = 10;
        const MESSAGE_COST = 5;
        const MESSAGE_LIMIT = 99;

        test('Create account with balance less than 0', () => {
            expect(()=>{
                let account = new MainAccount(-1, CALL_COST);
            }).toThrow(); 
        });

        test('Create account with balance equals 0', () => {
            let account = new MainAccount(0, CALL_COST, MESSAGE_COST, MESSAGE_LIMIT);
            expect(account.getBalance()).toEqual(0);    
        });

        var balance_amounts = [
            1, 5, 9, 10, 100, 1000, 100000, 1000000
        ];

        test('Create account with balance greater than 0', () => {
            balance_amounts.forEach(amount => {
                let account = new MainAccount(amount, CALL_COST, MESSAGE_COST, MESSAGE_LIMIT);
                expect(account.getBalance()).toEqual(amount);
            });
        });
    });

    describe('test call cost', () => {
        const ACCOUNT_BALANCE = 0;
        const MESSAGE_COST = 5;
        const MESSAGE_LIMIT = 99;

        test('Call cost is negative', () => {
            expect(()=>{
                let account = new MainAccount(ACCOUNT_BALANCE, -1, MESSAGE_COST, MESSAGE_LIMIT);
            }).toThrow(); 
        });

        test('Call cost is 0', () => {
            expect(()=>{
                let account = new MainAccount(ACCOUNT_BALANCE, 0, MESSAGE_COST, MESSAGE_LIMIT);
            }).toThrow();  
        });

        const call_costs = [
            1, 5, 9, 10, 100, 1000, 100000, 1000000
        ];

        test('Create account call cost greater than 0', () => {
            call_costs.forEach(amount => {
                let account = new MainAccount(ACCOUNT_BALANCE, amount, MESSAGE_COST, MESSAGE_LIMIT);
                expect(account.getUnitCallCost()).toEqual(amount);
            });
        });
    });

    describe('test sms cost', () => {
        const ACCOUNT_BALANCE = 0;
        const CALL_COST = 5;
        const MESSAGE_LIMIT = 99;

        test('SMS cost is negative', () => {
            expect(()=>{
                let account = new MainAccount(ACCOUNT_BALANCE, CALL_COST, -1, MESSAGE_LIMIT);
            }).toThrow(); 
        });

        test('sms cost is 0', () => {
            expect(()=>{
                let account = new MainAccount(ACCOUNT_BALANCE, CALL_COST, 0, MESSAGE_LIMIT);
            }).toThrow();  
        });

        const call_costs = [
            1, 5, 9, 10, 100, 1000, 100000, 1000000
        ];

        test('Create account with sms cost greater than 0', () => {
            call_costs.forEach(amount => {
                let account = new MainAccount(ACCOUNT_BALANCE, CALL_COST, amount, MESSAGE_LIMIT);
                expect(account.getUnitMessageCost()).toEqual(amount);
            });
        });
    });

    describe('test message limit', () => {
        const ACCOUNT_BALANCE = 0;
        const CALL_COST = 5;
        const SMS_COST = 10;

        test('message limit is negative', () => {
            expect(()=>{
                let account = new MainAccount(ACCOUNT_BALANCE, CALL_COST, SMS_COST, -10);
            }).toThrow(); 
        });

        test('message limit is 0', () => {
            expect(()=>{
                let account = new MainAccount(ACCOUNT_BALANCE, CALL_COST, SMS_COST, 0);
            }).toThrow();  
        });

        const call_costs = [
            1, 5, 9, 10, 50, 90, 99
        ];

        test('Create account with message limit greater than 0', () => {
            call_costs.forEach(amount => {
                let account = new MainAccount(ACCOUNT_BALANCE, CALL_COST, SMS_COST, amount);
                expect(account.getMessageLimit()).toEqual(amount);
            });
        });

        const msg_limit_greater_than_limit_costs = [
            100, 101, 200, 300
        ];

        test('Create account with message limit greater than 100 or equal', () => {
            msg_limit_greater_than_limit_costs.forEach(amount => {            
                expect(()=>{
                    let account = new MainAccount(ACCOUNT_BALANCE, CALL_COST, SMS_COST, amount);
                }).toThrow();             
            });
        });
    });

});


describe('topup balance test', () => {
    const ACCOUNT_BALANCE = 0;
    const CALL_COST = 5;
    const SMS_COST = 10;
    const MESSAGE_LIMIT = 99;

    test('topup negative balance', () => {
        expect(()=>{
            let account = new MainAccount(ACCOUNT_BALANCE, CALL_COST, SMS_COST, MESSAGE_LIMIT);

            account.topUpBalance(-1);
        }).toThrow(); 
    });

    test('topup zero balance', () => {
        expect(()=>{
            let account = new MainAccount(ACCOUNT_BALANCE, CALL_COST, SMS_COST, MESSAGE_LIMIT);

            account.topUpBalance(0);
        }).toThrow(); 
    });

    const topup_amounts = [
        1, 5, 9, 10, 50, 90, 99
    ];

    test('topup positive balance', () => {
        topup_amounts.forEach(amount => {
            let account = new MainAccount(ACCOUNT_BALANCE, CALL_COST, SMS_COST, MESSAGE_LIMIT);

            account.topUpBalance(amount);

            expect(account.getBalance()).toEqual(amount + ACCOUNT_BALANCE);
        }); 
    });
});


describe('deduce call test', () => {
    const ACCOUNT_BALANCE = 500;
    const CALL_COST = 10;
    const SMS_COST = 10;
    const MESSAGE_LIMIT = 99;

    test('sufficient balance', () => {
        let account = new MainAccount(ACCOUNT_BALANCE, CALL_COST, SMS_COST, MESSAGE_LIMIT);
        
        let callDuration = 10;
        account.deduceCall(callDuration);

        expect(account.getBalance()).toEqual(ACCOUNT_BALANCE - CALL_COST * callDuration);
    });


    test('no sufficient balance', () => {
        expect(()=>{
            let account = new MainAccount(ACCOUNT_BALANCE, CALL_COST, SMS_COST, MESSAGE_LIMIT);
            
            let callDuration = 60;
            account.deduceCall(callDuration);
    
        }).toThrow();
    });
});

describe('deduce sms test', () => {
    const ACCOUNT_BALANCE = 250;
    const CALL_COST = 10;
    const SMS_COST = 10;
    const MESSAGE_LIMIT = 50;

    test('sufficient balance', () => {
        let account = new MainAccount(ACCOUNT_BALANCE, CALL_COST, SMS_COST, MESSAGE_LIMIT);
        
        let smsLength = 150;
        account.deduceSMS(smsLength);

        let cost = Math.ceil(smsLength / MESSAGE_LIMIT) * SMS_COST;
        expect(account.getBalance()).toEqual(ACCOUNT_BALANCE - cost);
    });


    test('no sufficient balance', () => {
        expect(()=>{
            let account = new MainAccount(ACCOUNT_BALANCE, CALL_COST, SMS_COST, MESSAGE_LIMIT);
        
            let smsLength = 10000;
            account.deduceSMS(smsLength);
    
        }).toThrow();
    });
});

describe('deduce data test', () => {
    const ACCOUNT_BALANCE = 250;
    const CALL_COST = 10;
    const SMS_COST = 10;
    const MESSAGE_LIMIT = 50;

    test('sufficient balance', () => {
        let account = new MainAccount(ACCOUNT_BALANCE, CALL_COST, SMS_COST, MESSAGE_LIMIT);
        
        let dataSize = 15;
        account.deduceData(dataSize);

        let cost = dataSize * SMS_COST; // data amd sms costs are the same in the main account
        expect(account.getBalance()).toEqual(ACCOUNT_BALANCE - cost);
    });


    test('no sufficient balance', () => {
        expect(()=>{
            let account = new MainAccount(ACCOUNT_BALANCE, CALL_COST, SMS_COST, MESSAGE_LIMIT);
        
            let dataSize = 10000;
            account.deduceData(dataSize);
    
        }).toThrow();
    });
});