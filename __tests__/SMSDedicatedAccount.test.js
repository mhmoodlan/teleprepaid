import { SMSDedicatedAccount } from './../src/SMSDedicatedAccount.js';

describe('Test SMS Dedicated account', () => {

    test('Test creation', () => {
        expect(() => {
            let balance = 10;
            let unitCost = 5;
            let messageLimit = 25;
            let smsDedicatedAccount = new SMSDedicatedAccount(balance, unitCost, messageLimit);
        }).not.toThrow();
    });

    test('Test get type', () => {
        expect(() => {
            let balance = 10;
            let unitCost = 5;
            let messageLimit = 25;
            let smsDedicatedAccount = new SMSDedicatedAccount(balance, unitCost, messageLimit);

            expect(smsDedicatedAccount.getType()).toEqual('sms');
        }).not.toThrow();
    });

    test('Test set type', () => {
        expect(() => {
            let balance = 10;
            let unitCost = 5;
            let messageLimit = 25;
            let smsDedicatedAccount = new SMSDedicatedAccount(balance, unitCost, messageLimit);

            smsDedicatedAccount.setType('call');
        }).toThrow('Dedicated account can not change type!');
    });

    test('Test get message limit', () => {
        expect(() => {
            let balance = 10;
            let unitCost = 5;
            let messageLimit = 25;
            let smsDedicatedAccount = new SMSDedicatedAccount(balance, unitCost, messageLimit);

            expect(smsDedicatedAccount.getMessageLimit()).toEqual(messageLimit);
        }).not.toThrow();
    });

    test('Test set message limit', () => {
        expect(() => {
            let balance = 10;
            let unitCost = 5;
            let messageLimit = 25;
            let smsDedicatedAccount = new SMSDedicatedAccount(balance, unitCost, messageLimit);

            let newMessageLimit = 50;
            smsDedicatedAccount.setMessageLimit(newMessageLimit);

            expect(smsDedicatedAccount.getMessageLimit()).toEqual(newMessageLimit);
        }).not.toThrow();
    });

    test('Test set invalid message limit', () => {
        let balance = 10;
        let unitCost = 5;
        let messageLimit = 25;
        let smsDedicatedAccount = new SMSDedicatedAccount(balance, unitCost, messageLimit);

        let newMessageLimit = -50;

        expect(() => smsDedicatedAccount.setMessageLimit(newMessageLimit)).toThrow('Message limit must be a number greater than 0!');
    });

    describe('test deduce SMS', () => {
        test('sufficient balance', () => {
            expect(() => {
                let balance = 20;
                let unitCost = 10;
                let messageLimit = 50;
                let smsDedicatedAccount = new SMSDedicatedAccount(balance, unitCost, messageLimit);

                let smsLength = 100;
                smsDedicatedAccount.deduceSMS(smsLength);
                expect(smsDedicatedAccount.getBalance()).toEqual(balance - unitCost * Math.ceil(smsLength / messageLimit));

            }).not.toThrow();
        });
        // Test to detect tricky case #2
        test('Test deduce sms cost calculation', () => {
            let balance = 20;
            let unitCost = 10;
            let messageLimit = 33;
            let smsDedicatedAccount = new SMSDedicatedAccount(balance, unitCost, messageLimit);

            let smsLength = 50;
            smsDedicatedAccount.deduceSMS(smsLength);

            expect(smsDedicatedAccount.getBalance()).toEqual(balance - unitCost * Math.ceil(smsLength / messageLimit));
        });

        test('non-sufficient balance', () => {
            expect(() => {
                let balance = 20;
                let unitCost = 10;
                let messageLimit = 50;
                let smsDedicatedAccount = new SMSDedicatedAccount(balance, unitCost, messageLimit);

                let smsLength = 150;
                smsDedicatedAccount.deduceSMS(smsLength);

            }).toThrow('sms dedicated account does not have enough balance to perform this action!');
        });
    });

});