import { GSMUser } from './../src/GSMUser.js';
import { CallDedicatedAccount } from './../src/CallDedicatedAccount.js';
import { MainAccount } from './../src/MainAccount.js';
import { SMSDedicatedAccount } from './../src/SMSDedicatedAccount.js';
import { DataDedicatedAccount } from './../src/DataDedicatedAccount.js';

describe('Test Call Dedicated account', () => {


    let main = new MainAccount(10, 5, 2, 99);

    test('Test creation', () => {
        expect(() => {
            let balance = 10;
            let unitCost = 5;
            let callDedicated = new CallDedicatedAccount(balance, unitCost);
        }).not.toThrow();
    });

    test('Test get balance', () => {
        expect(() => {
            let balance = 10;
            let unitCost = 5;
            let callDedicated = new CallDedicatedAccount(balance, unitCost);
            expect(callDedicated.getBalance()).toEqual(balance);
        }).not.toThrow();
    });

    test('Test get type', () => {
        expect(() => {
            let balance = 10;
            let unitCost = 5;
            let callDedicated = new CallDedicatedAccount(balance, unitCost);
            
            expect(callDedicated.getType()).toEqual('call');
        }).not.toThrow();
    });

    test('Test set type', () => {
        expect(() => {
            let balance = 10;
            let unitCost = 5;
            let callDedicated = new CallDedicatedAccount(balance, unitCost);
            
            callDedicated.setType('sms');
        }).toThrow();
    });


    describe('Test Call Dedicated account', () => {
        test('sufficient balance', () => {
            expect(() => {
                let balance = 10;
                let unitCost = 5;
                let callDedicated = new CallDedicatedAccount(balance, unitCost);
                
                let call_duration = 2;
                callDedicated.deduceCall(call_duration);

                expect(callDedicated.getBalance()).toEqual(balance - unitCost * call_duration);

            }).not.toThrow();
        });

        test('non-sufficient balance', () => {
            expect(() => {
                let balance = 10;
                let unitCost = 5;
                let callDedicated = new CallDedicatedAccount(balance, unitCost);
                
                let call_duration = 3;
                callDedicated.deduceCall(call_duration);
            }).toThrow();
        });
    });

});