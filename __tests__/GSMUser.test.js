import { GSMUser } from './../src/GSMUser.js';
import { CallDedicatedAccount } from './../src/CallDedicatedAccount.js';
import { MainAccount } from './../src/MainAccount.js';
import { SMSDedicatedAccount } from './../src/SMSDedicatedAccount.js';
import { DataDedicatedAccount } from './../src/DataDedicatedAccount.js';
import { DedicatedAccount } from '../src/DedicatedAccount.js';


describe('Test GSMUser', () => {

    jest.mock('../src/SMSDedicatedAccount');
    jest.mock('../src/CallDedicatedAccount');
    jest.mock('../src/DataDedicatedAccount');
    jest.mock('../src/MainAccount');
    let main = new MainAccount(10, 5, 2, 99);
    let callDedicated = new CallDedicatedAccount(10, 5);
    let SMSDedicated = new SMSDedicatedAccount(10, 5, 50);
    let dataDedicated = new DataDedicatedAccount(30, 3);

    describe('Test GSMUser constructor', () => {

        test('Test create GSMUser without parameters', () => {
            expect(() => new GSMUser()).toThrow('mainAccount should be an instance of MainAccount!');
        });

        test('Test create GSMUser with wrong main account type', () => {
            expect(() => new GSMUser(callDedicated)).toThrow('mainAccount should be an instance of MainAccount!');
        });

        test('Test create GSMUser with correct main account type', () => {
            expect(() => new GSMUser(main)).not.toThrow();
        });

        test('Test create GSMUser with correct main account type and wrong dedicated account type', () => {
            expect(() => new GSMUser(main, {'call': new DedicatedAccount(100, 2)})).toThrow('dedicatedAccounts elements should be instances of DedicatedAccount and one of the allowed types!');
            expect(() => new GSMUser(main, {'call': new String('call')}).toThrow('dedicatedAccounts elements should be instances of DedicatedAccount and one of the allowed types!'));
            expect(() => new GSMUser(main, 123)).toThrow('dedicatedAccounts should be an instance of Object!');
        });

        test('Test create GSMUser with full parameters', () => {
            expect(() => new GSMUser(main, { 'call': callDedicated, 'sms': SMSDedicated, 'data': dataDedicated })).not.toThrow();
        });
    });

    describe('Test get GSMUser balance', () => {
        let user = new GSMUser(main);
        test('Test get GSMUser balance with only main account', () => {
            expect(user.getBalance().main).toBe(10);
        });

        test('Test get GSMUser balance with main and call accounts', () => {
            user = new GSMUser(main, { 'call': callDedicated });
            expect(user.getBalance()).toEqual(expect.objectContaining({ 'main': 10, 'dedicated': [{ 'call': 10 }] }));
        });
        test('Test get GSMUser balance with main, call, and sms accounts', () => {
            user = new GSMUser(main, { 'call': callDedicated, 'sms': SMSDedicated });
            expect(user.getBalance()).toEqual(expect.objectContaining({ 'main': 10, 'dedicated': [{ 'call': 10, 'sms': 10 }] }));
        });
        test('Test get GSMUser balance with all accounts', () => {
            user = new GSMUser(main, { 'call': callDedicated, 'sms': SMSDedicated, 'data': dataDedicated });
            expect(user.getBalance()).toEqual(expect.objectContaining({ 'main': 10, 'dedicated': [{ 'call': 10, 'sms': 10, 'data': 30 }] }));
        });
    });

    describe('Test GSMUser topup balance', () => {
        let user = new GSMUser(main);
        test('Test GSMUser topup balance with main account only', () => {
            user.topUpBalance(500);
            expect(user.getBalance()).toEqual(expect.objectContaining({ 'main': 510, 'dedicated': [{}] }));
        });

        test('Test GSMUser topup balance with main and dedicated accounts', () => {
            main = new MainAccount(10, 5, 2, 99);
            user = new GSMUser(main, { 'call': callDedicated, 'sms': SMSDedicated, 'data': dataDedicated });
            user.topUpBalance(500);
            expect(user.getBalance()).toEqual(expect.objectContaining({ 'main': 510, 'dedicated': [{ 'call': 10, 'sms': 10, 'data': 30 }] }));
        });

        test('Test GSMUser topup balance with invalid amount', () => {
            expect(() => user.topUpBalance(-1)).toThrow();
        });

        test('Test GSMUser topup balance with missing amount', () => {
            expect(() => user.topUpBalance()).toThrow('Amount should be a number!');
        });

        test('Test GSMUser topup balance with null amount', () => {
            expect(() => user.topUpBalance(null)).toThrow('Amount should be a number!');
        });
    });

    describe('Test GSMUser add dedicated accounts', () => {
        let user = new GSMUser(main);
        test('Test add dedicated accounts with missing type', () => {
            expect(() => user.addDedicatedAccount(main)).toThrow('type and/or account cannot be undefined!');
        });

        test('Test add dedicated accounts with missing account', () => {
            expect(() => user.addDedicatedAccount(undefined, 'call')).toThrow('type and/or account cannot be undefined!');
        });

        test('Test add dedicated accounts with wrong type', () => {
            expect(() => user.addDedicatedAccount(callDedicated, 'wrong')).toThrow('Account of type wrong is not valid!');
        });

        test('Test add main account as dedicated account', () => {
            expect(() => user.addDedicatedAccount(main, 'call')).toThrow('Only accounts instances of DedicatedAccount can be added as dedicated accounts!');
        });

        test('Test add dedicated accounts that already exists', () => {
            user = new GSMUser(main, {'call': callDedicated});
            expect(() => user.addDedicatedAccount(callDedicated, 'call')).toThrow('Failed to add account. Dedicated account of type call already active for this user!');
        });
    });

    describe('Test GSMUser remove dedicated account', () => {
        let user = new GSMUser(main);
        test('Test remove dedicated account with missing type', () => {
            expect(() => user.removeDedicatedAccount()).toThrow('Failed to remove account. Missing type or user does not have a dedicated account of type undefined');
        });
        test('Test remove dedicated account with wrong type', () => {
            expect(() => user.removeDedicatedAccount('wrong')).toThrow('Failed to remove account. Missing type or user does not have a dedicated account of type wrong');
        });
        test('Test remove dedicated account', () => {
            user.addDedicatedAccount(callDedicated, 'call');
            expect(() => user.removeDedicatedAccount('call')).not.toThrow();
        });
    });

    describe('Test GSMUser call', () => {
        let user = new GSMUser(main);
        test('Test call with missing duration', () => {
            expect(() => user.call()).toThrow('Missing duration!');
        });

        test('Test call with invalid duration', () => {
            expect(() => user.call('duration')).toThrow('Invalid duration!');
        });

        test('Test call with negative duration', () => {
            expect(() => user.call(-10)).toThrow('Invalid duration!');
        });

        test('Test call with boundary duration', () => {
            expect(() => user.call(1000)).toThrow('Invalid duration!');
        });

        test('Test call with valid duration', () => {
            expect(() => user.call(10)).not.toThrow();
        });

        test('Test call with valid duration but not enough balance', () => {
            expect(user.call(100)).toEqual('Main account does not have enough balance to make a 100 minute(s) call');
        });

        test('Test call with valid duration with call dedicated account', () => {
            user.addDedicatedAccount(callDedicated, 'call');
            expect(user.call(1)).toEqual('1 mins call cost was deduced from call dedicated account!');
        });

        test('Test call with valid duration with call dedicated account but not enough dedicated balance', () => {
            expect(user.call(10)).toEqual('Call dedicated account does not have enough balance to make a 10 minute(s) call, switching to main account!');
        });
    });

    describe('Test GSMUser send SMS', () => {
        let user = new GSMUser(main);
        test('Test SMS with missing length', () => {
            expect(() => user.sendSMS()).toThrow('Missing length!');
        });

        test('Test SMS with invalid length', () => {
            expect(() => user.sendSMS('length')).toThrow('Invalid length!');
        });

        test('Test SMS with negative length', () => {
            expect(() => user.sendSMS(-10)).toThrow('Invalid length!');
        });

        test('Test SMS with boundary length', () => {
            expect(() => user.sendSMS(1000)).toThrow('Invalid length!');
        });

        test('Test SMS with valid length', () => {
            expect(() => user.sendSMS(10)).not.toThrow();
        });

        test('Test SMS with valid length but not enough main balance', () => {
            let main = new MainAccount(10, 5, 2, 99);
            let user = new GSMUser(main);
            expect(user.sendSMS(550)).toEqual('Main account does not have enough balance to send a 550 long SMS!');
        });

        test('Test SMS with valid length and SMS dedicated account', () => {
            user.addDedicatedAccount(SMSDedicated, 'sms');
            expect(() => user.sendSMS(10)).not.toThrow();
        });

        test('Test SMS with valid length and SMS dedicated account but not enough dedicated balance', () => {
            expect(user.sendSMS(99)).toEqual('SMS dedicated account does not have enough balance to send a 99 long SMS, switching to main account!');
        });
    });

    describe('Test GSMUser use data', () => {
        let user = new GSMUser(main);
        test('Test data with missing size', () => {
            expect(() => user.useData()).toThrow('Missing data size!');
        });
        
        //Test catching the first tricky case, it should fail!
        test('Test use data with 0 parameter', () => {
            expect(() => user.useData(0)).toThrow('Invalid data size!');
        });

        test('Test data with invalid size', () => {
            expect(() => user.useData('size')).toThrow('Invalid data size!');
        });

        test('Test data with negative size', () => {
            expect(() => user.useData(-10)).toThrow('Invalid data size!');
        });

        test('Test data with boundary size', () => {
            expect(() => user.useData(10000)).toThrow('Invalid data size!');
        });

        test('Test data with valid size', () => {
            expect(() => user.useData(10)).not.toThrow();
        });

        test('Test data with valid size but not enough main balance', () => {
            expect(user.useData(500)).toEqual('Main account does not have enough balance to use 500 sized data!');
        });

        test('Test data with valid size and data dedicated account', () => {
            user.addDedicatedAccount(dataDedicated, 'data');
            expect(() => user.useData(10)).not.toThrow();
        });

        test('Test data with valid size and data dedicated account but not enough dedicated balance', () => {
            expect(user.useData(500)).toEqual('Data dedicated account does not have enough balance to use a 500 sized data, switching to main account!');
        });
    });
});