import { DedicatedAccount } from '../src/DedicatedAccount.js';

describe('Test Dedicated account', () => {

    test('Test creation', () => {
        expect(() => {
            let balance = 10;
            let unitCost = 5;
            let dedicated = new DedicatedAccount(balance, unitCost);
        }).not.toThrow();
    });

    test('Test get unit cost', () => {
        expect(() => {
            let balance = 10;
            let unitCost = 5;
            let dedicated = new DedicatedAccount(balance, unitCost);
            
            expect(dedicated.getUnitCost()).toEqual(unitCost);

        }).not.toThrow();
    });

    test('Test set unit cost', () => {
        expect(() => {
            let balance = 10;
            let unitCost = 5;
            let dedicated = new DedicatedAccount(balance, unitCost);
            
            let newUnitCost = 20;
            dedicated.setUnitCost(newUnitCost);

            expect(dedicated.getUnitCost()).toEqual(newUnitCost);

        }).not.toThrow();
    });

    test('Test set invalid unit cost', () => {
        
        let balance = 10;
        let unitCost = 5;
        let dedicated = new DedicatedAccount(balance, unitCost);
        
        let newUnitCost = -20;

        expect(() => dedicated.setUnitCost(newUnitCost)).toThrow('Unit cost must be a number greater than 0!');
    });
});