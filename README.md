# TelePrepaid

### Requirements
- [Install NodeJS with NPM](https://nodejs.dev/download)

### Clone repository and install dependencies
```shell
git clone https://gitlab.com/mhmoodlan/teleprepaid
cd teleprepaid
npm install
```

### Run application
```shell
npm run serve
```
then open `http://127.0.0.1:8080` in the browser.

### Run test
```shell
npm run test
```

### Check test coverage
```shell
npx jest --coverage
```
