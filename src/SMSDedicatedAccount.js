import { DedicatedAccount } from './DedicatedAccount.js';

export class SMSDedicatedAccount extends DedicatedAccount {
    constructor(balance, unitCost, messageLimit) {
        super(balance, unitCost);
        this.type = "sms";
        this.setMessageLimit(messageLimit);
    }

    getType() {
        return this.type;
    }

    setType(type) {
        throw "Dedicated account can not change type!" // TODO: potential source of error
    }

    getMessageLimit() {
        return this.messageLimit;
    }

    setMessageLimit(messageLimit) {
        if (messageLimit > 0) {
            this.messageLimit = messageLimit;
        } else {
            throw 'Message limit must be a number greater than 0!';
        }
    }

    deduceSMS(length) {
        /***
         * Tricky case #2:
         * Cost calculation should always result in an integer.
         * Without using Math.ceil or Math.floor, the result of the following calculation
         * will give a float, which is against the desired behavior!
         * This tricky case is detected by the test: "Test deduce sms cost calculation"
         * The fix is to set cost as follows: let cost = Math.ceil(length / this.messageLimit) * this.unitCost;

         */
        let cost = (length / this.messageLimit) * this.unitCost;
        if (this.balance >= cost) {
            this.balance -= cost;
            return;
        } else {
            throw `${this.type} dedicated account does not have enough balance to perform this action!`;
        }
    }
}
