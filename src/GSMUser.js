import { MainAccount } from './MainAccount.js';
import { DedicatedAccount } from './DedicatedAccount.js';
import { CallDedicatedAccount } from './CallDedicatedAccount.js';
import { SMSDedicatedAccount } from './SMSDedicatedAccount.js';
import { DataDedicatedAccount } from './DataDedicatedAccount.js';

export class GSMUser {

    constructor(mainAccount, dedicatedAccounts) {
        this.dedicatedAccounts = {};
        if (mainAccount instanceof MainAccount) {
            this.mainAccount = mainAccount;
        }
        else throw 'mainAccount should be an instance of MainAccount!';
        if (dedicatedAccounts !== undefined) {
            if (dedicatedAccounts.constructor === Object) {
                for (let account in dedicatedAccounts)
                    if (DedicatedAccount.allowedDedicatedAccounts.indexOf(account) === -1 ||
                        !(Object.getPrototypeOf(Object.getPrototypeOf(dedicatedAccounts[account])).constructor.name === "DedicatedAccount"))
                        throw 'dedicatedAccounts elements should be instances of DedicatedAccount and one of the allowed types!'
                this.dedicatedAccounts = dedicatedAccounts;
            }
            else throw 'dedicatedAccounts should be an instance of Object!'
        }
    }

    getBalance() {
        let dedicatedBalances = [];
        let dedicatedBalancesObj = {};
        for (let account in this.dedicatedAccounts) {
            dedicatedBalancesObj[this.dedicatedAccounts[account].getType()] = this.dedicatedAccounts[account].getBalance();
        }
        dedicatedBalances.push(dedicatedBalancesObj);
        return {
            'main': this.mainAccount.getBalance(),
            'dedicated': dedicatedBalances
        };
    }

    topUpBalance(amount) {
        if (typeof amount === 'number' && amount.toString() !== "NaN") {
            this.mainAccount.topUpBalance(amount);
        } else {
            throw 'Amount should be a number!';
        }
    }

    addDedicatedAccount(account, type) {
        if (account && type) {
            if (DedicatedAccount.allowedDedicatedAccounts.indexOf(type) !== -1) {
                if (type in this.dedicatedAccounts) {
                    throw `Failed to add account. Dedicated account of type ${type} already active for this user!`;
                }
            } else {
                throw `Account of type ${type} is not valid!`
            }
        } else {
            throw 'type and/or account cannot be undefined!';
        }
        if (Object.getPrototypeOf(Object.getPrototypeOf(account)).constructor.name === 'DedicatedAccount') {
            this.dedicatedAccounts[type] = account;
        } else {
            throw 'Only accounts instances of DedicatedAccount can be added as dedicated accounts!';
        }
    }

    removeDedicatedAccount(type) {
        if (!type || !(type in this.dedicatedAccounts)) {
            throw `Failed to remove account. Missing type or user does not have a dedicated account of type ${type}`;
        }
        delete this.dedicatedAccounts[type];
    }

    call(duration) {
        if (duration !== undefined) {
            if (typeof duration !== "number" || duration >= 200 || duration <= 0)
                throw 'Invalid duration!'
        } else {
            throw 'Missing duration!';
        }
        if (this.dedicatedAccounts['call'] instanceof CallDedicatedAccount) {
            try {
                this.dedicatedAccounts['call'].deduceCall(duration);
                return `${duration} mins call cost was deduced from call dedicated account!`;
            } catch (e) {
                return `Call dedicated account does not have enough balance to make a ${duration} minute(s) call, switching to main account!`;
            }
        }
        try {
            this.mainAccount.deduceCall(duration);
            return `${duration} mins call cost was deduced from main account!`;
        } catch (e) {
            return `Main account does not have enough balance to make a ${duration} minute(s) call`;
        }
    }

    sendSMS(length) {
        if (length !== undefined) {
            if (typeof length !== "number" || length >= 750 || length <= 0)
                throw 'Invalid length!'
        } else {
            throw 'Missing length!';
        }
        if (this.dedicatedAccounts['sms'] instanceof SMSDedicatedAccount) {
            try {
                this.dedicatedAccounts['sms'].deduceSMS(length);
                return `Cost of an SMS with length ${length} was deduced from SMS dedicated account!`;
            } catch (e) {
                return `SMS dedicated account does not have enough balance to send a ${length} long SMS, switching to main account!`;
            }
        }
        try {
            this.mainAccount.deduceSMS(length);
            return `Cost of an SMS with length ${length} was deduced from main account!`;
        } catch (e) {
            return `Main account does not have enough balance to send a ${length} long SMS!`;
        }
    }

    useData(data) {
        /*** 
         * // Tricky #1:
         * the condition is to check if the parameter is not undefined (i.e., missing) however, 
         * if the user passes 0, even though the parameter exists,
         * however the condition fails to detect it and the wrong exception is thrown!
         * The fix is to replace the if(data) condition with: if(data !== undefined)
         * This error is catched by the: "Test use data with 0 parameter"
         * ***/
        if (data) { 
            if (typeof data !== "number" || data >= 1000 || data <= 0)
                throw 'Invalid data size!'
        } else {
            throw 'Missing data size!';
        }
        if (this.dedicatedAccounts['data'] instanceof DataDedicatedAccount) {
            try {
                this.dedicatedAccounts['data'].deduceData(data);
                return `Cost of using data with size ${data} was deduced from data dedicated account!`;
            } catch (e) {
                return `Data dedicated account does not have enough balance to use a ${data} sized data, switching to main account!`;
            }
        }
        try {
            this.mainAccount.deduceData(data);
            return `Cost of using data with size ${data} was deduced from main account`;
        } catch (e) {
            return `Main account does not have enough balance to use ${data} sized data!`;
        }
    }
}