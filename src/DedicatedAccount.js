import { Account } from './Account.js';

export class DedicatedAccount extends Account {
    constructor(balance, unitCost) {
        super(balance);
        this.setUnitCost(unitCost);
    }

    static allowedDedicatedAccounts = ['call', 'sms', 'data'];

    
    getUnitCost() {
        return this.unitCost;
    }

    setUnitCost(unitCost) {
        if(unitCost > 0) {
            this.unitCost = unitCost;
        } else {
            throw 'Unit cost must be a number greater than 0!';
        }
    }
}
