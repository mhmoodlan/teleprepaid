import { DedicatedAccount } from './DedicatedAccount.js';

export class DataDedicatedAccount extends DedicatedAccount {
    constructor(balance, unitCost) {
        super(balance, unitCost);
        this.type = "data";
    }

    getType() {
        return this.type;
    }

    setType(type) {
        throw "Dedicated account can not change type!" // TODO: potential source of error
    }

    deduceData(data) {
        let cost = data * this.unitCost;
        if (this.balance >= cost) {
            this.balance -= cost;
            return;
        } else {
            throw `${this.type} dedicated account does not have enough balance to perform this action!`;
        }
    }
}
