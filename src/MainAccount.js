import { Account } from './Account.js';

export class MainAccount extends Account {
    constructor(balance, unitCallCost, unitMessageCost, messageLimit) {
        super(balance);
        this.setUnitCallCost(unitCallCost);
        this.setUnitMessageCost(unitMessageCost);
        this.setMessageLimit(messageLimit);
    }

    setUnitCallCost(unitCallCost) {
        if (unitCallCost > 0)
            this.unitCallCost = unitCallCost;
        else
            throw 'Call cost should be greater than 0!';
    }

    setUnitMessageCost(unitMessageCost) {
        if (unitMessageCost > 0)
            this.unitMessageCost = unitMessageCost;
        else
            throw 'Message cost should be greater than 0!';
    }

    setMessageLimit(messageLimit) {
        if (messageLimit > 0 && messageLimit < 100)
            this.messageLimit = messageLimit;
        else
            throw 'Message limit should be greater than 0 and less than 100!';
    }

    getUnitCallCost() {
        return this.unitCallCost;
    }

    getUnitMessageCost() {
        return this.unitMessageCost;
    }

    getMessageLimit() {
        return this.messageLimit;
    }

    topUpBalance(amount) {
        if (amount > 0)
            this.balance += amount;
        else
            throw 'Top up amount should be greater than 0!';
    }

    deduceCall(duration) {
        let cost = duration * this.unitCallCost;
        if (this.balance >= cost) {
            this.balance -= cost;
            return;
        } else {
            throw `Main account does not have enough balance to perform this action!`;
        }
    }

    deduceSMS(length) {
        let cost = Math.ceil(length / this.messageLimit) * this.unitMessageCost; // TODO: Possible tricky case
        if (this.balance >= cost) {
            this.balance -= cost;
            return;
        } else {
            throw `Main account does not have enough balance to perform this action!`;
        }
    }

    deduceData(data) {
        let cost = data * this.unitMessageCost; // data and message costs are the same in main account
        if (this.balance >= cost) {
            this.balance -= cost;
            return;
        } else {
            throw `Main account does not have enough balance to perform this action!`;
        }
    }
}
