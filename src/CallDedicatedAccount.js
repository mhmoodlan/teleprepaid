import { DedicatedAccount } from './DedicatedAccount.js';

export class CallDedicatedAccount extends DedicatedAccount {
    constructor(balance, unitCost) {
        super(balance, unitCost);
        this.type = "call";
    }

    getType() {
        return this.type;
    }

    setType(type) {
        throw "Dedicated account can not change type!" // TODO: potential source of error
    }

    deduceCall(duration) {
        let cost = duration * this.unitCost;
        if (this.balance >= cost) {
            this.balance -= cost;
            return;
        } else {
            throw `${this.type} dedicated account does not have enough balance to perform this action!`;
        }
    }
}
