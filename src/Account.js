export class Account {
    constructor(balance) {
        this.setBalance(balance);
    }

    setBalance(balance) {
        if(balance >= 0)
            this.balance = balance;
        else
            throw 'Balance should not be less than 0!';
    }

    getBalance() {
        return this.balance;
    }
}